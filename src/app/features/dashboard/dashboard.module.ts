import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { TRANSLOCO_LANG, TRANSLOCO_SCOPE, TranslocoModule } from '@ngneat/transloco';
import { LayoutsModule } from '../../layouts/layouts.module';

const routes: Routes = [{ path: '', component: DashboardComponent }];

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, RouterModule.forChild(routes), TranslocoModule, LayoutsModule],
  providers: [
    { provide: TRANSLOCO_SCOPE, useValue: 'dashboard' },
    {
      provide: TRANSLOCO_LANG,
      useValue: 'vi'
    }
  ]
})
export class DashboardModule {}
