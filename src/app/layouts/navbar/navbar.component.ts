import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TRANSLOCO_LANG, TRANSLOCO_SCOPE, TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [
    { provide: TRANSLOCO_SCOPE, useValue: 'layouts' },
    { provide: TRANSLOCO_LANG, useValue: 'en' }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
  get activeLang(): string {
    return this._translocoService.getActiveLang();
  }

  constructor(private _translocoService: TranslocoService) {}

  ngOnInit() {}

  onChangeLang(lang: string = 'en') {
    if (lang === 'en') {
      this._translocoService.setActiveLang('en');
    }
    if (lang === 'vi') {
      this._translocoService.setActiveLang('vi');
    }
  }
}
