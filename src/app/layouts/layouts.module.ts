import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
  declarations: [NavbarComponent],
  imports: [CommonModule, TranslocoModule],
  exports: [NavbarComponent]
})
export class LayoutsModule {}
